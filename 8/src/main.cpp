#include <iostream>
#include "Pair.h"
#include "Money.h"
#include "Complex.h"

int main()
{
    Money a(42, 88), b(1, 14);
    Pair *c = &(a + b);
    std::cout << c->first << " " << c->second << std::endl;

    Complex d(100, 85), e(12, 24);
    Pair *f = &(d + e);
    std::cout << f->first << " " << f->second << std::endl;

    return 0;
}
