#include <typeinfo>
#include "Pair.h"
#include "Complex.h"
#include "TypeMismatch.h"

Complex::Complex(int real, int imagine) : Pair(real, imagine)
{
}

Pair& Complex::operator +(const Pair &other)
{
    const Complex* otherComplex = dynamic_cast<const Complex*>(&other);
    if (otherComplex == 0)
        throw new TypeMismatch();
    Complex* sum = new Complex(this->first + otherComplex->first, this->second + otherComplex->second);
    return *sum;
}
