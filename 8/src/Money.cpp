#include <typeinfo>
#include "Pair.h"
#include "Money.h"
#include "TypeMismatch.h"

Money::Money(int dollars, int cents) : Pair(dollars, cents)
{
    this->first += this->second / 100;
    this->second %= 100;
}

Pair& Money::operator +(const Pair &other)
{
    const Money* otherMoney = dynamic_cast<const Money*>(&other);
    if (otherMoney == 0)
        throw new TypeMismatch();
    Money* sum = new Money(this->first + otherMoney->first, this->second + otherMoney->second);
    return *sum;
}
