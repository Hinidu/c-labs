#ifndef PAIR_H
#define PAIR_H

class Pair
{
    public:
    int first, second;

    Pair(int first, int second);

    virtual Pair& operator +(const Pair &other) = 0;
};

#endif
