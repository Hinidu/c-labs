#ifndef MONEY_H
#define MONEY_H

#include "Pair.h"

class Money : public Pair
{
    public:
    Money(int dollars, int cents);

    virtual Pair& operator +(const Pair &other);
};

#endif
