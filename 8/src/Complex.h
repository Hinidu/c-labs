#ifndef COMPLEX_H
#define COMPLEX_H

#include "Pair.h"

class Complex : public Pair
{
    public:
    Complex(int real, int imagine);

    virtual Pair& operator +(const Pair &other);
};

#endif
