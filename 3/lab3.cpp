#include <iostream>
#include <algorithm>

using namespace std;

void swap_cols(int **a, int n, int j, int k)
{
    while (n--)
        swap(*(*(a + n) + j), *(*(a + n) + k));
}

int sum_col(int **a, int n, int j)
{
    int sum = 0;
    while (n--)
        sum += *(*(a + n) + j);
    return sum;
}

int main()
{
    int n;
    cin >> n;
    int** a = new int*[n];
    for (int i = 0; i < n; ++i)
        a[i] = new int[n];

    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            cin >> a[i][j];

    bool has_neg = false;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < i; ++j)
            has_neg |= a[i][j] < 0;

    if (has_neg)
    {
        int j = 0, k = 0, sumj = sum_col(a, n, 0), sumk = sumj;
        for (int p = 1; p < n; ++p)
        {
            int sum = sum_col(a, n, p);
            if (sumj > sum)
            {
                j = p;
                sumj = sum;
            }
            if (sumk < sum)
            {
                k = p;
                sumk = sum;
            }
        }
        swap_cols(a, n, j, k);
    }

    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
            cout << a[i][j] << " ";
        cout << endl;
    }

    for (int i = 0; i < n; ++i)
        delete a[i];
    delete a;

    return 0;
}
