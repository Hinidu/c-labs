#ifndef DEQUE_H
#define DEQUE_H

#include <list>

template <class T>
class Deque
{
    private:
        std::list<T> list;
        std::size_t m_size;

    public:
        Deque() : list(std::list<T>()), m_size(0)
        {
        }

        class DequeIsEmpty { };

        void push_front(T x)
        {
            this->list.insert(list.begin(), x);
            ++this->m_size;
        }

        void push_back(T x)
        {
            this->list.insert(list.end(), x);
            ++this->m_size;
        }

        T pop_front()
        {
            if (this->empty())
                throw new DequeIsEmpty();
            T x = *this->list.begin();
            this->list.erase(this->list.begin());
            --this->m_size;
            return x;
        }

        T pop_back()
        {
            if (this->empty())
                throw new DequeIsEmpty();
            T x = *this->list.rbegin();
            this->list.erase(this->list.rbegin());
            --this->m_size;
            return x;
        }

        std::size_t size()
        {
            return m_size;
        }

        bool empty()
        {
            return this->size() == 0;
        }
};

#endif
