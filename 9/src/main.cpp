#include "Deque.h"
#include <iostream>
#include <string>

int main()
{
    Deque<int> d_ints;
    d_ints.push_back(42);
    d_ints.push_front(-1);
    for (int i = 0; i < 3; ++i)
    {
        try
        {
            std::cout << d_ints.pop_front() << std::endl;
        }
        catch (Deque<int>::DequeIsEmpty ex)
        {
            std::cout << "ERROR! Deque is empty!" << std::endl;
        }
    }

    Deque<double> d_doubles;
    d_doubles.push_back(3.14);

    Deque<std::string> d_strings;
    d_strings.push_front("C++");

    return 0;
}
