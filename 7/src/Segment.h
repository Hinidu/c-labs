#ifndef SEGMENT_H
#define SEGMENT_H

#include "Point.h"

class Segment
{
    public:
        Point *a, *b;

        Segment(Point* a, Point* b);

        ~Segment();

        bool intersect(Segment* segment);
};

#endif
