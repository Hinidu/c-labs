#ifndef VECTOR_H
#define VECTOR_H

#include "Point.h"
#include "Segment.h"

class Vector
{
    public:
        int x, y;
        
        Vector(int x, int y);

        Vector(Point* point);

        Vector(Point* from, Point* to);

        Vector(Segment* segment);
};

int operator *(const Vector &v1, const Vector &v2);

#endif
