#include <stdexcept>
#include "Point.h"
#include "Segment.h"

Segment::Segment(Point* a, Point* b)
{
    if (a == NULL)
        throw std::invalid_argument("a in Segment(Point*, Point*)");
    this->a = new Point(*a);
    if (b == NULL)
        throw std::invalid_argument("b in Segment(Point*, Point*)");
    this->b = new Point(*b);
}

Segment::~Segment()
{
    delete a;
    delete b;
}


bool Segment::intersect(Segment* segment)
{
    int cx1 = this->a->x - segment->a->x;
    int cx2 = this->b->x - this->a->x - segment->b->x + segment->a->x;
    int cy1 = this->a->y - segment->a->y;
    int cy2 = this->b->y - this->a->y - segment->b->y + segment->a->y;
    double tx = 1.0 * cx1 / cx2;
    double ty = 1.0 * cy1 / cy2;
    return cx1 * cy2 == cx2 * cy1 && 0 <= tx && tx <= 1 && 0 <= ty && ty <= 1;
}
