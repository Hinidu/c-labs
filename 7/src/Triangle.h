#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <iostream>
#include "Point.h"
#include "Shape.h"

class Triangle : public Shape
{
    public:
        Triangle(Point **points);

    friend std::ostream& operator <<(std::ostream &out, const Triangle &t);
};

#endif
