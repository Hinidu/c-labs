#ifndef SHAPE_H
#define SHAPE_H

#include <stdexcept>
#include "Point.h"
#include "Segment.h"

class Shape
{
    public:
        Point **points;
        int cnt;

    public:
        Shape(Point **points, int cnt);

        ~Shape();

        int get_cnt() const;

        Segment* get_side(int i);

        virtual void move(int dx, int dy);

        virtual bool contains(Point* point);

        virtual bool intersect(Shape* shape);

        friend std::ostream& operator <<(std::ostream &out, const Shape &sh);
};

#endif
