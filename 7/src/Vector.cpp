#include <stdexcept>
#include "Point.h"
#include "Vector.h"
#include "Segment.h"

Vector::Vector(int x, int y) : x(x), y(y)
{
}

Vector::Vector(Point* point)
{
    if (point == NULL)
        throw std::invalid_argument("point in Vector(Point*)");

    this->x = point->x;
    this->y = point->y;
}

Vector::Vector(Point* from, Point* to)
    : Vector(new Segment(from, to))
{
}

Vector::Vector(Segment* segment)
{
    if (segment == NULL)
        throw std::invalid_argument("segment in Vector(Segment*)");

    this->x = segment->b->x - segment->a->x;
    this->y = segment->b->y - segment->a->y;
}

int operator *(const Vector &v1, const Vector &v2)
{
    return v1.x * v2.y - v1.y * v2.x;
}
