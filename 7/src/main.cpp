#include <iostream>
#include "Point.h"
#include "Triangle.h"
#include "Rectangle.h"

int main()
{
    Point **t_ps = new Point*[3];
    std::cout << "Write triangle points: ";
    for (int i = 0; i < 3; ++i)
    {
        int x, y;
        std::cin >> x >> y;
        t_ps[i] = new Point(x, y);
    }
    Triangle *t = new Triangle(t_ps);
    std::cout << (*t) << std::endl;

    Point **r_ps = new Point*[4];
    std::cout << "Write rectangle points: ";
    for (int i = 0; i < 4; ++i)
    {
        int x, y;
        std::cin >> x >> y;
        r_ps[i] = new Point(x, y);
    }
    Rectangle *r = new Rectangle(r_ps);
    std::cout << (*r) << std::endl;

    std::cout << "Intersect " << t->intersect(r) << std::endl;

    return 0;
}
