#include <iostream>
#include <stdexcept>
#include "Rectangle.h"
#include "Point.h"
#include "Shape.h"

Rectangle::Rectangle(Point **points) : Shape(points, 4)
{
    if (this->get_cnt() != 4)
        throw std::invalid_argument("points in Rectangle(Point**)");
}

std::ostream& operator <<(std::ostream &out, const Rectangle &r)
{
    out << "Rectangle:" << std::endl;
    for (int i = 0; i < r.get_cnt(); ++i)
        out << r.points[i]->x << " " << r.points[i]->y << std::endl;
    return out;
}
