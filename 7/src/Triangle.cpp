#include <iostream>
#include <stdexcept>
#include "Triangle.h"
#include "Point.h"
#include "Shape.h"

Triangle::Triangle(Point **points) : Shape(points, 3)
{
    if (this->get_cnt() != 3)
        throw std::invalid_argument("points in Triangle(Point**)");
}

std::ostream& operator <<(std::ostream &out, const Triangle &t)
{
    out << "Triangle:" << std::endl;
    for (int i = 0; i < t.get_cnt(); ++i)
        out << t.points[i]->x << " " << t.points[i]->y << std::endl;
    return out;
}
