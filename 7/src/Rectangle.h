#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
#include "Point.h"
#include "Shape.h"

class Rectangle : public Shape
{
    public:
        Rectangle(Point **points);

    friend std::ostream& operator <<(std::ostream &out, const Rectangle &r);
};

#endif
