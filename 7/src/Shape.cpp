#include <iostream>
#include <stdexcept>
#include "Point.h"
#include "Vector.h"
#include "Shape.h"

Shape::Shape(Point **points, int cnt) : points(points), cnt(cnt)
{
    if (this->cnt < 3)
        throw std::invalid_argument("points in Shape(Point**)");
}

Shape::~Shape()
{
    for (Point **p = this->points; *p != NULL; ++p)
        delete p;
}

int Shape::get_cnt() const
{
    return this->cnt;
}

Segment* Shape::get_side(int i)
{
    if (i >= this->cnt)
        throw std::invalid_argument("i");
    return new Segment(this->points[i], this->points[i + 1 == this->cnt ? 0 : i + 1]);
}

void Shape::move(int dx, int dy)
{
    for (Point **p = this->points; *p != NULL; ++p)
    {
        (*p)->x += dx;
        (*p)->y += dy;
    }
}

bool Shape::contains(Point* point)
{
    if (point == NULL)
        throw std::invalid_argument("point in Shape::contains(Point*)");

    //std::cout << "Contains cnt = " << this->cnt << std::endl;
    //for (int i = 0; i < this->cnt; ++i)
        //std::cout << this->points[i]->x << " " << this->points[i]->y << std::endl;
    bool pos = false, neg = false;
    for (int i = 0; i < this->cnt; ++i)
    {
        Segment *side = get_side(i);
        Vector *v_side = new Vector(side);
        Vector *to_point = new Vector(this->points[i], point);

        int product = (*v_side) * (*to_point);
        //std::cout << "side " << side->a->x << " " << side->a->y << " " << side->b->x << " " << side->b->y << std::endl;
        //std::cout << "vectors " << v_side->x << " " << v_side->y << " " << to_point->x << " " << to_point->y << " " << product << std::endl;
        if (product < 0)
            neg = true;
        else if (product > 0)
            pos = true;

        delete side;
        delete v_side;
        delete to_point;
    }
    return !(pos && neg);
}

bool Shape::intersect(Shape* shape)
{
    for (int i = 0; i < this->cnt; ++i)
        if (shape->contains(this->points[i]))
            return true;

    for (int j = 0; j < shape->cnt; ++j)
        if (this->contains(shape->points[j]))
            return true;

    for (int i = 0; i < this->cnt; ++i)
    {
        Segment *ab = this->get_side(i);
        for (int j = 0; j < shape->cnt; ++j)
        {
            Segment *cd = shape->get_side(j);
            if (ab->intersect(cd))
                return true;
            delete cd;
        }
        delete ab;
    }

    return false;
}

std::ostream& operator <<(std::ostream& out, const Shape &sh)
{
    for (int i = 0; i < sh.cnt; ++i)
        out << sh.points[i]->x << " " << sh.points[i]->y << std::endl;
    return out;
}
