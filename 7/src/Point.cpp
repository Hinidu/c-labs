#include "Point.h"

Point::Point(int x, int y) : x(x), y(y)
{
}

Point::Point(const Point &p) : x(p.x), y(p.y)
{
}
