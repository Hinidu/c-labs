#include <cstdio>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <vector>

using namespace std;

struct notebook
{
    static constexpr float BIG_DIAGONAL = 11.0f;

    char model[21];

    int price;
    float weight;

    struct box_size
    {
        float x;
        float y;
        float z;
    } size;

    int cpu_freq;
    int ram;
    float diag;
    int vram;

    struct disp_res
    {
        int x;
        int y;
    } disp_res;

    int disp_freq;
    double hdd;
};

istream& operator >>(istream &in, notebook &note)
{
    in.read(note.model, 20);
    note.model[20] = '\0';
    char buf[15];
    in >> note.price >> note.weight >> buf;
    sscanf(buf, "%fx%fx%f", &note.size.z, &note.size.y, &note.size.x);
    in >> note.cpu_freq >> note.ram >> note.diag >> note.vram >> buf;
    sscanf(buf, "%dx%d", &note.disp_res.y, &note.disp_res.x);
    in >> note.disp_freq >> note.hdd;
    return in;
}

ostream& operator <<(ostream &out, const notebook &note)
{
    out << fixed
        << setfill('0')

        << note.model

        << setw(4)
        << note.price << ' '

        << setprecision(1)
        << setw(3)
        << note.weight << ' '

        << setw(4)
        << note.size.z
        << 'x'
        << setw(4)
        << note.size.y
        << 'x'
        << setw(4)
        << note.size.x << ' '

        << setw(3)
        << note.cpu_freq << ' '

        << setw(2)
        << note.ram << ' '

        << setw(4)
        << note.disp_res.y
        << 'x'
        << setw(4)
        << note.disp_res.x << ' '

        << setw(2)
        << note.disp_freq << ' '

        << setprecision(3)
        << setw(5)
        << note.hdd;

    return out;
}

int main()
{
    ifstream txt_in("note.txt", std::ios::in);
    vector<notebook> big_notes;
    for (notebook note; txt_in >> note; )
        if (note.diag >= notebook::BIG_DIAGONAL)
            big_notes.push_back(note);
    txt_in.close();

    ofstream bin_out("big_note.bin", std::ios::out | std::ios::binary);
    size_t count = big_notes.size();
    bin_out.write((char*)&count, 2);
    bin_out.write((char*)&*big_notes.begin(), count * sizeof(notebook));
    bin_out.close();

    ifstream bin_in("big_note.bin", std::ios::in | std::ios::binary);
    bin_in.read((char*)&count, 2);
    big_notes.resize(count);
    bin_in.read((char*)&*big_notes.begin(), count * sizeof(notebook));
    bin_in.close();

    ofstream txt_out("big_note.txt", std::ios::out);
    txt_out << count;
    for (vector<notebook>::const_iterator it = big_notes.begin(); it != big_notes.end(); ++it)
        txt_out << *it;
    txt_out.close();

    return 0;
}
