#include <stdlib.h>
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

bool is_palindrom(const string &s)
{
    string::const_iterator i = s.begin(), j = s.end();
    for (--j; i < j; ++i, --j)
        if (*i != *j)
            return false;
    return true;
}

string double_char(const string &s, char ch)
{
    string t = "";
    for (string::const_iterator it = s.begin(); it != s.end(); ++it)
    {
        t.push_back(*it);
        if (*it == ch)
            t.push_back(ch);
    }
    return t;
}

char* lltoa(long long x, char *s = 0, int base = 10)
{
    if (base < 2 || base > 36)
    {
        cerr << "lltoa: incorrect base value " << base << endl;
        exit(1);
    }
    if (!s)
        s = (char*)malloc(64 * sizeof(char));
    char *i = s;
    if (x < 0)
    {
        *i++ = '-';
        x = -x;
    }
    while (x)
    {
        long long y = x / base, rem = x - y * base;
        *i++ = (rem < 10 ? '0' : 'A' - 10) + rem;
        x = y;
    }
    *i-- = '\0';
    char *j = s;
    if (*j == '-')
        ++j;
    for (; i > j; --i, ++j)
        swap(*i, *j);
    return s;
}

bool is_prime(long long x)
{
    if (x < 2)
        return false;
    for (long long i = 2; i * i <= x; ++i)
        if (x % i == 0)
            return false;
    return true;
}

int main()
{
    string s;
    cin >> s;
    if (is_palindrom(s))
    {
        long long x = atoll(s.c_str());
        if (x <= 2)
            cout << "No primes less than " << x << endl;
        else
        {
            while (!is_prime(--x));
            cout << x << endl;
        }
    }
    else
    {
        s = double_char(double_char(s, '0'), '8');
        long long x = atoll(s.c_str());
        char *cs = lltoa(x, 0, 5);
        cout << cs << endl;
        free(cs);
    }
    return 0;
}
