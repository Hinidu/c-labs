#include <iostream>
#include <iterator>
#include <functional>
#include <algorithm>

using namespace std;

const int MAXN = 40;

int a[MAXN];

struct even_digits_count_comparator : public binary_function<int, int, bool>
{
    private:
    int even_digits_count(int x) const
    {
        if (x == 0)
            return 1;
        if (x < 0)
            x = -x;
        int count = 0;
        while (x)
        {
            int y = x / 10;
            count += ((x - y * 10) & 1) ^ 1;
            x = y;
        }
        return count;
    }
    
    public:
    bool operator ()(int x, int y) const
    {
        return even_digits_count(x) < even_digits_count(y);
    }
};

int main()
{
    int *b = copy(istream_iterator<int>(cin), istream_iterator<int>(), a);
    int *p = a;
    while (p < b && *p++ >= 0);
    sort(a, p, greater<int>());
    sort(p, b, even_digits_count_comparator());
    copy(a, b, ostream_iterator<int>(cout, " "));
    return 0;
}
