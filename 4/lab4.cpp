#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cctype>

FILE *A, *B, *C;

FILE* target_file(const char* word)
{
    static char vowels[] = "aeiou";
    int cnt_con = 0, cnt_vow = 0;
    while (*word)
        ++(strchr(vowels, *word++) ? cnt_vow : cnt_con);
    return cnt_vow < cnt_con ? A : cnt_vow == cnt_con ? B : C;
}

int main()
{
    A = fopen("A.txt", "w");
    B = fopen("B.txt", "w");
    C = fopen("C.txt", "w");

    char* word = NULL;
    int length = 0;
    char ch;
    while ((ch = getchar()) != EOF)
    {
        if (isalpha(ch))
        {
            ++length;
            word = (char*)realloc(word, (length + 1) * sizeof(char));
            word[length - 1] = ch;
            word[length] = '\0';
        }
        else if (word != NULL)
        {
            fprintf(target_file(word), "%s\n", word);
            free(word);
            word = NULL;
            length = 0;
        }
    }

    fclose(A);
    fclose(B);
    fclose(C);
    return 0;
}
