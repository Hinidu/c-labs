#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include "book.pb.h"

namespace hinidu
{
    namespace homelibrary
    {
        class Constraint
        {
            public:
                const char *title_part;
                const char *author_part;
                const Book::Style style;
                const int fromYear;
                const int toYear;
                const char **genres;
                const int genres_count;

                Constraint(const char*, const char*, const Book::Style,
                        const int, const int, const char**, const int);

                bool satisfy(const Book&) const;
        };
    }
}

#endif
