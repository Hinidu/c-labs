#include <iostream>
#include <algorithm>
#include <functional>

#include "homelibrary.h"
#include "book.pb.h"

namespace hinidu
{
    namespace homelibrary
    {
        int HomeLibrary::size() const
        {
            return this->library_base.book_size();
        }

        void HomeLibrary::add(const Book &book)
        {
            Book* newBook = this->library_base.add_book();
            newBook->CopyFrom(book);
        }

        Book HomeLibrary::book(int i) const
        {
            return this->library_base.book(i);
        }

        HomeLibrary HomeLibrary::filter(const Constraint& constraint)
        {
            HomeLibrary filteredLibrary;
            for (int i = 0; i < this->size(); ++i)
            {
                Book curBook = this->book(i);
                if (constraint.satisfy(curBook))
                    filteredLibrary.add(curBook);
            }
            return filteredLibrary;
        }

        bool HomeLibrary::ParseFromIstream(std::istream *stream)
        {
            return this->library_base.ParseFromIstream(stream);
        }

        bool HomeLibrary::SerializeToOstream(std::ostream *stream) const
        {
            return this->library_base.SerializeToOstream(stream);
        }
    }
}
