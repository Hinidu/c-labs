#ifndef HOMELIBRARY_H
#define HOMELIBRARY_H

#include <iostream>
#include "homelibrary.pb.h"
#include "book.pb.h"
#include "constraint.h"

namespace hinidu
{
    namespace homelibrary
    {
        class HomeLibrary
        {
            private:
                HomeLibraryBase library_base;

            public:
                int size() const;

                void add(const Book &book);

                Book book(int i) const;

                HomeLibrary filter(const Constraint &constraint);

                template <class Compare>
                    void stable_sort(Compare compare);

                bool ParseFromIstream(std::istream *stream);

                bool SerializeToOstream(std::ostream *stream) const;
        };

        template <class Compare>
            void HomeLibrary::stable_sort(Compare compare)
            {
                std::stable_sort(this->library_base.mutable_book()->begin(), this->library_base.mutable_book()->end(), compare);
            }
    }
}

#endif
