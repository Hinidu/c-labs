#include <fstream>
#include <set>
#include <algorithm>
#include <ncurses.h>
#include <cdk.h>
#include "book.pb.h"
#include "homelibrary.h"

#define LIBRARY_FILENAME "home.library"
#define BOOK_COLS 10

using namespace hinidu::homelibrary;

HomeLibrary library; 

CDK_PARAMS params;

CDKSCREEN *cdkScreen;
CDKMATRIX *bookTable;
CDKENTRY *findTitleEntry, *findAuthorEntry;
CDKITEMLIST *findStyleList;
CDKSCALE *findMinYearScale, *findMaxYearScale;
CDKSELECTION *findGenreSelection;
WINDOW *cursesWin;

int min_year = 0, max_year, cur_year;
unsigned int genres_cnt = 0, genres_used = 0;
char **genres;

bool readLibrary();

void writeLibrary();

void initData();

void createFindWidgets();

void updateFindWidgets();

void createBookTable();

void fillBookTable(const HomeLibrary &library);

void exitProgram(int code);

int main(int argc, char **argv)
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    CDKparseParams(argc, argv, &params, "trcT:" CDK_MIN_PARAMS);

    cursesWin = initscr();
    cdkScreen = initCDKScreen(cursesWin);

    initCDKColor();

    if (!readLibrary())
        exitProgram(EXIT_FAILURE);

    initData();

    createFindWidgets();
    createBookTable();
    fillBookTable(library);

    drawCDKScreen(cdkScreen);
    traverseCDKScreen(cdkScreen);

    writeLibrary();

    exitProgram(EXIT_SUCCESS);
}

bool readLibrary()
{
    bool ok = true;
    std::fstream library_file(LIBRARY_FILENAME, std::ios::in | std::ios::binary);
    if (!library_file)
    {
        const char *message[3] =
        {
            "<C>Library does not exist.",
            "<C>Creating a new library.",
            "<C>Press any key to continue."
        };
        popupLabel(cdkScreen, (CDK_CSTRING2)message, 3);
    }
    else if (!library.ParseFromIstream(&library_file))
    {
        const char *message[3] =
        {
            "<C>Failed to read home library information from " LIBRARY_FILENAME ".",
            "<C>You can remove it and create a new one.",
            "<C>If you don't want remove corrupted library.dat select exit."
        };
        const char *buttons[2] = { "Create new library", "Exit" };
        CDKDIALOG *dialog = newCDKDialog(cdkScreen, CENTER, CENTER,
                (CDK_CSTRING2)message, 3,
                (CDK_CSTRING2)buttons, 2,
                A_REVERSE, TRUE,
                TRUE, FALSE);
        int answer = activateCDKDialog(dialog, 0);
        destroyCDKDialog(dialog);
        ok = answer == 0;
    }
    library_file.close();
    return ok;
}

void writeLibrary()
{
    std::fstream library_file(LIBRARY_FILENAME, std::ios::out | std::ios::binary);
    library.SerializeToOstream(&library_file);
    library_file.close();
}

void initData()
{
    time_t clck = time(0);
    max_year = 0;
    min_year = cur_year = 1900 + localtime(&clck)->tm_year;

    std::set<std::string> set_genres;
    for (int i = 0; i < library.size(); ++i)
    {
        Book book = library.book(i);
        min_year = std::min(min_year, book.year());
        max_year = std::max(max_year, book.year());
        for (int j = 0; j < book.genre_size(); ++j)
            set_genres.insert(book.genre(j));
    }

    max_year = std::max(max_year, min_year);

    for (std::set<std::string>::const_iterator it = set_genres.begin(); it != set_genres.end(); ++it)
        genres_used = CDKallocStrings(&genres, const_cast<char*>((*it).c_str()), genres_cnt++, genres_used);
}

void createFindWidgets()
{
    findTitleEntry = newCDKEntry(cdkScreen, 0, 0,
            "<C>Enter a part of title", "</U>Title:<!U> ",
            A_NORMAL, ' ',
            vMIXED, 31,
            0, 64,
            TRUE, FALSE);

    findAuthorEntry = newCDKEntry(cdkScreen, 0, 4,
            "<C>Enter a part of author name", "</U>Author:<!U> ",
            A_NORMAL, ' ',
            vMIXED, 30,
            0, 64,
            TRUE, FALSE);

    const char *styleNames[4];
    styleNames[0] = "ANY";
    styleNames[1] = Book::Style_Name(Book::ART).c_str();
    styleNames[2] = Book::Style_Name(Book::PUBLICISM).c_str();
    styleNames[3] = Book::Style_Name(Book::SCIENCE).c_str();
    findStyleList = newCDKItemlist(cdkScreen, 0, 8,
            "Pick a style", 0,
            (CDK_CSTRING2)styleNames, 4, 0,
            TRUE, FALSE);

    findMinYearScale = newCDKScale(cdkScreen, 14, 8,
            "Pick a year", "</U>From:<!U> ",
            A_NORMAL,
            5,
            min_year, min_year, cur_year,
            1, 10,
            TRUE, FALSE);

    findMaxYearScale = newCDKScale(cdkScreen, 27, 8,
            "Pick a year", "</U>To:<!U> ",
            A_NORMAL,
            5,
            max_year, min_year, max_year,
            1, 10,
            TRUE, FALSE);

    const char *choices[2] = { " ", "*" };
    findGenreSelection = newCDKSelection(cdkScreen, 0, 12,
            RIGHT,
            10, 39,
            "<C>Select genres",
            (CDK_CSTRING2)genres, genres_cnt,
            (CDK_CSTRING2)choices, 2,
            A_NORMAL,
            TRUE, FALSE);
}

void updateFindWidgets()
{
}

void createBookTable()
{
    int rows             = 1;
    int cols             = 5;
    int vrows            = 8;
    int vcols            = 5;

    const char *coltitle[BOOK_COLS];
    const char *rowtitle[BOOK_COLS];

    int colwidth[BOOK_COLS];
    int colvalue[BOOK_COLS];

#define set_col(n, width, string) \
    coltitle[n] = string;\
    colwidth[n] = width ;\
    colvalue[n] = vUMIXED

    set_col (1, 20, "</B/5>Title");
    set_col (2, 20, "</B/33>Author");
    set_col (3, 9, "</B/33>Style");
    set_col (4, 10, "</B/33>Genres");
    set_col (5, 4, "</B/7>Year");

    for (int i = 1; i <= rows; ++i)
        rowtitle[i] = 0;

    bookTable = newCDKMatrix (cdkScreen, 40, 0,
            rows, cols, vrows, vcols,
            "<C>Books",
            (CDK_CSTRING2) rowtitle,
            (CDK_CSTRING2) coltitle,
            colwidth, colvalue,
            -1, -1, ' ',
            COL,
            TRUE, FALSE, FALSE); 
}

void fillBookTable(const HomeLibrary &library)
{
}

void exitProgram(int code)
{
    CDKfreeStrings(genres);
    destroyCDKEntry(findTitleEntry);
    destroyCDKEntry(findAuthorEntry);
    destroyCDKItemlist(findStyleList);
    destroyCDKScale(findMinYearScale);
    destroyCDKScale(findMaxYearScale);
    destroyCDKMatrix(bookTable);
    destroyCDKScreen(cdkScreen);
    endCDK();

    google::protobuf::ShutdownProtobufLibrary();

    exit(code);
}
