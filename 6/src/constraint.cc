#include <string.h>
#include "book.pb.h"
#include "constraint.h"

namespace hinidu
{
    namespace homelibrary
    {
        Constraint::Constraint(const char *title_part, const char *author_part,
                const Book::Style style, const int fromYear, const int toYear,
                const char **genres, const int genres_count)
            : title_part(title_part), author_part(author_part), style(style),
              fromYear(fromYear), toYear(toYear), genres(genres),
              genres_count(genres_count)
        {
        }

        bool Constraint::satisfy(const Book &book) const
        {
            if (this->title_part != 0 && strstr(book.title().c_str(), this->title_part) == 0)
                return false;
            if (this->author_part != 0 && strstr(book.author().c_str(), this->author_part) == 0)
                return false;
            if ((this->style & book.style()) != book.style())
                return false;
            if (this->fromYear > book.year() || this->toYear < book.year())
                return false;
            if (this->genres_count)
            {
                for (int i = 0; i < book.genre_size(); ++i)
                {
                    const char *book_genre = book.genre(i).c_str();
                    for (int j = 0; j < genres_count; ++j)
                        if (strcmp(book_genre, this->genres[j]))
                            return true;
                }
                // Book has not any of requested genres
                return false;
            }
            return true;
        }
    }
}
